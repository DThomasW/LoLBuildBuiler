(function (app){
  'use strict';
  app = angular.module("RiotPrac", ["ngRoute", "ngSanitize"]);
  app.config(["$routeProvider", function($routeProvider){
    $routeProvider
    .when("/Home", {
      controller:"mainCtrl",
      templateUrl: "html/main.html"
    })
    .when("/:id", {
      controller: "champCtrl",
      templateUrl: "html/champion.html"
    })
    .otherwise("/Home");
  }]);
  
  RiotPrac = app;
})(window.RiotPrac = window.RiotPrac || {});