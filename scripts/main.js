(function (app){
  'use script';
  
   app.service("apiService", function($http, $q){
    this.getChamps = function(){
      var url = 'https://na.api.pvp.net/api/lol/static-data/na/v1.2/champion?champData=all&api_key=RGAPI-C6FB6A6E-3356-4964-8DE4-43325FADA12C';
      var list = [];
      var deferred = $q.defer();
      return $http.get(url)
      .then(function(res){
        deferred.resolve(res.data.data);
        return deferred.promise;
      }, function(res){
        deferred.reject(res); 
        return deferred.promise;});
    };
    
  });
  
  app.controller("mainCtrl", function($scope, $http, apiService){
    apiService.getChamps()
    .then(
      function(result){
        $scope.champList = result;
      }, function(error){
        console.log(error);
      }); 
  });
  
})(window.RiotPrac);