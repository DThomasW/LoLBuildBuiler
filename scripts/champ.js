(function (app){
  'use script';
     app.service("champSer", function($http, $q){
     this.getChampData = function(id){
      var url = 'https://na.api.pvp.net/api/lol/static-data/na/v1.2/champion/'+id+'?champData=all&api_key=RGAPI-C6FB6A6E-3356-4964-8DE4-43325FADA12C'
      var obj = {};
      var defCData = $q.defer();
      return $http.get(url)
      .then(function(res){
        defCData.resolve(res.data);
        return defCData.promise;
      }, function(res){defCData.reject(res);
        return defCData.promise;}); 
        };
        
      this.getItems = function(id){
      var url = 'https://na.api.pvp.net/api/lol/static-data/na/v1.2/item?itemListData=all&api_key=RGAPI-C6FB6A6E-3356-4964-8DE4-43325FADA12C';
      var defIData = $q.defer();
      return $http.get(url)
      .then(function(res){
        defIData.resolve(res.data);
        return defIData.promise;
      }, function(res){defIData.reject(res);
        return defIdata.promise;});
      };
      
      this.getBuildItem = function(id){
        var url = 'https://na.api.pvp.net/api/lol/static-data/na/v1.2/item/'+id+'?itemData=all&api_key=RGAPI-C6FB6A6E-3356-4964-8DE4-43325FADA12C';
        var defIData = $q.defer();
        return $http.get(url)
        .then(function(res){
          defIData.resolve(res.data);
          return defIData.promise;
        }, function(res){ defIData.reject(res);
          return defIData.promise;});
      };
      
     });
     
app.controller("champCtrl", function($scope, $http, $q, $routeParams, champSer){
   $scope.champId = $routeParams.id;
   $scope.ChampLevel = 0;
   $scope.ChampStat = {
     HP:0, Res: 0, HPReg: 0, ResReg: 0,
     Atk: 0, AP: 0, Crit: 0, AtkSpd: 0,
     AtkPen: 0, APPen: 0, Armor: 0, MagRes: 0,
     Move: 0, CDR: 0 
   };
   $scope.Build = [];
   champSer.getChampData($scope.champId)
       .then(
         function(result){
           $scope.CurrChamp = result;
           var tSpd = 0.625/ (1 + $scope.CurrChamp.stats.attackspeedoffset);
           $scope.atkSpd = Number(tSpd.toString().match(/^\d+(?:\.\d{0,3})?/));
           $scope.SpdPerLvl = result.stats.attackspeedperlevel;
           //console.log($scope.CurrChamp);
         }, function(err){
           console.log(err);
         });
   champSer.getItems()
   .then(function(result){
     $scope.ItemList = result.data;
     console.log($scope.ItemList);
     }, function(err){
       console.log(err);
     });
         
    $scope.GrowStat = function(base, growth){
      var s = (base + growth*($scope.ChampLevel - 1)*(0.685 + (0.0175*$scope.ChampLevel)));
      return Number(s.toString().match(/^\d+(?:\.\d{0,3})?/));
    };
    $scope.GrowAtkSpeed = function(base, growth){
      var s = (growth*($scope.ChampLevel - 1)*(0.685 + (0.0175*$scope.ChampLevel)));
      var sp = (base + (base*(s/100)));
      return Number(sp.toString().match(/^\d+(?:\.\d{0,3})?/));      
    };
    
    $scope.CalcStats = function(){
      if($scope.ChampLevel != 0){
        $scope.ChampStat = {
          HP: $scope.GrowStat($scope.CurrChamp.stats.hp, $scope.CurrChamp.stats.hpperlevel), 
          Res: $scope.GrowStat($scope.CurrChamp.stats.mp, $scope.CurrChamp.stats.mpperlevel), 
          HPReg: $scope.GrowStat($scope.CurrChamp.stats.hpregen, $scope.CurrChamp.stats.hpregenperlevel),
          ResReg: $scope.GrowStat($scope.CurrChamp.stats.mpregen, $scope.CurrChamp.stats.mpregenperlevel),
          Atk: $scope.GrowStat($scope.CurrChamp.stats.attackdamage, $scope.CurrChamp.stats.attackdamageperlevel), 
          AP: 0, 
          Crit: 0, 
          AtkSpd: $scope.GrowAtkSpeed($scope.atkSpd, $scope.SpdPerLvl),
          AtkPen: 0, 
          APPen: 0, 
          Armor: $scope.GrowStat($scope.CurrChamp.stats.armor, $scope.CurrChamp.stats.armorperlevel), 
          MagRes: $scope.GrowStat($scope.CurrChamp.stats.spellblock, $scope.CurrChamp.stats.spellblockperlevel),
          Move: $scope.CurrChamp.stats.movespeed, 
          CDR: 0 
        };
      }
      $scope.ItemStat(); 
    };
         
    $scope.ChangeLevel = function(num){
      if(num == 1 && $scope.ChampLevel < 18){
        $scope.ChampLevel += 1;
      }
      else if(num == 1 && $scope.ChampLevel == 18){
        $scope.ChampLevel = 1;
      }
      else if(num == 0 && $scope.ChampLevel > 1){
        $scope.ChampLevel -= 1;
      }
      else if(num == 0 && $scope.ChampLevel == 1){
        $scope.ChampLevel = 18;
      }
      
      $scope.CalcStats();
    };
    
     $scope.ItemStat = function(){
        
        for(var x = 0; x < $scope.Build.length; x++){
          if($scope.Build[x].stats.FlatHPPoolMod){            
           $scope.ChampStat.HP += $scope.Build[x].stats.FlatHPPoolMod;
          }
           if($scope.CurrChamp.parType == "MP" && $scope.Build[x].stats.FlatMPPoolMod){
             $scope.ChampStat.Res += $scope.Build[x].stats.FlatMPPoolMod;
           }
           if($scope.Build[x].stats.FlatPhysicalDamageMod){             
           $scope.ChampStat.Atk += $scope.Build[x].stats.FlatPhysicalDamageMod;
           }
           if($scope.Build[x].stats.FlatMagicDamageMod){             
           $scope.ChampStat.AP += $scope.Build[x].stats.FlatMagicDamageMod;
           }
           if($scope.Build[x].stats.FlatCritChanceMod){             
           $scope.ChampStat.Crit += ($scope.Build[x].stats.FlatCritChanceMod*100);
            if($scope.ChampStat.Crit > 100){ $scope.ChampStat.Crit = 100; }
           }
           if($scope.Build[x].stats.PercentAttackSpeedMod){             
           $scope.ChampStat.AtkSpd += ($scope.atkSpd*$scope.Build[x].stats.PercentAttackSpeedMod);
           }
           if($scope.Build[x].stats.FlatArmorMod){             
           $scope.ChampStat.Armor += $scope.Build[x].stats.FlatArmorMod;
           }
           if($scope.Build[x].stats.FlatSpellBlockMod){             
           $scope.ChampStat.MagRes += $scope.Build[x].stats.FlatSpellBlockMod;
           }
           //console.log($scope.Build[x]);
        }
        
      };
    
     $scope.AddBuild = function(itemId){
        if($scope.Build.length < 6){
          champSer.getBuildItem(itemId)
          .then(function(result){
          $scope.Build.push(result);
            //console.log(nItem);
          }, function(err){
            console.log(err);  
          });          
          $scope.CalcStats();
        }      
      };
      
     $scope.DelBuild = function(id){
       $scope.Build.splice(id, 1);
       $scope.CalcStats();
     }
    
});
     
})(window.RiotPrac);